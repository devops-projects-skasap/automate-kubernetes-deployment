module "myapp-eks-cluster" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.16.0"

  cluster_name = "myapp-eks-cluster"
  cluster_version = "1.27"

  subnet_ids = module.myapp-eks-vpc.private_subnets
  vpc_id = module.myapp-eks-vpc.vpc_id

  cluster_endpoint_public_access = true

  eks_managed_node_groups = {
    dev = {
      min_size     = 1
      max_size     = 3
      desired_size = 3

      instance_types = ["t2.small"]
    }
  }

  tags = {
    Environment = "Development"
    Application = "MyApp"
  }
}

resource "null_resource" "kubectl" {
    provisioner "local-exec" {
        command = "aws eks --region eu-west-1 update-kubeconfig --name ${module.myapp-eks-cluster.cluster_name} --kubeconfig ./kubeconfig"
    }
}
