## Automate Kubernetes Deployment via Ansible

### Technologies used:

Terraform, AWS EKS, Kubernetes, Docker, Linux, Python, Git

### Project Description:

1. Create EKS cluster with Terraform

2. Write an Ansible Play to deploy application in a new K8s namespace

### Usage Instructions:

#### Step 1: Assign values to the required variables (see `variables.tf`) in `terraform.tfvars` to create a K8s Cluster in AWS EKS, for example:

```
vpc_cidr_block = "10.0.0.0/16"

private_subnet_cidr_blocks = ["10.0.10.0/24", "10.0.20.0/24", "10.0.30.0/24"]
public_subnet_cidr_blocks = ["10.0.40.0/24", "10.0.50.0/24", "10.0.60.0/24"]
```

#### Step 2: Leverage Terraform modules to create an VPC covering all availability zones in a region and an EKS cluster with three `t2.small` nodes in that VPC:

```
data "aws_availability_zones" "azs" {}

module "myapp-eks-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.1.1"

  name = "myapp-eks-vpc"

  cidr = var.vpc_cidr_block

  private_subnets = var.private_subnet_cidr_blocks
  public_subnets = var.public_subnet_cidr_blocks

  azs = data.aws_availability_zones.azs.names

  enable_nat_gateway = true
  single_nat_gateway = true

  enable_dns_hostnames = true

  tags = { # Tags for VPC
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
  }

  private_subnet_tags = { # Tags for Private Subnets
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/internal-elb" = 1
  }

  public_subnet_tags = { # Tags for Public Subnets
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/elb" = 1
  }
}
```

```
module "myapp-eks-cluster" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.16.0"

  cluster_name = "myapp-eks-cluster"
  cluster_version = "1.27"

  subnet_ids = module.myapp-eks-vpc.private_subnets
  vpc_id = module.myapp-eks-vpc.vpc_id

  cluster_endpoint_public_access = true

  eks_managed_node_groups = {
    dev = {
      min_size     = 1
      max_size     = 3
      desired_size = 3

      instance_types = ["t2.small"]
    }
  }

  tags = {
    Environment = "Development"
    Application = "MyApp"
  }
}
```

#### Step 3: Create VPC and EKS cluster by executing:

```
cd terraform 

terraform init

terraform apply
```

##### Samples for the EKS cluster:

![image](images/eks-1.png)

![image](images/eks-2.png)

![image](images/eks-3.png)


#### Step 4: The `kubeconfig` file for the EKS cluster is configured by locally executing `aws eks update-kubeconfig` command via the Terraform `null_resource` resource:

```
resource "null_resource" "kubectl" {
    provisioner "local-exec" {
        command = "aws eks --region eu-west-1 update-kubeconfig --name ${module.myapp-eks-cluster.cluster_name} --kubeconfig ./kubeconfig"
    }
}
```

##### Access the `kubeconfig` file:

```
cat ./terraform/kubeconfig
```

#### Step 5: Compose a K8 manifest file `nginx.yaml` to create  `deployment` and `service` components for the Nginx application:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 3
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
        - name: nginx
          image: nginx
          ports:
            - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  ports:
    - name: http
      port: 80
      protocol: TCP
      targetPort: 80
  selector:
    app: nginx
  type: LoadBalancer
```

#### Step 6: Install the Python libraries of `kubernetes`, `PyYAML` and `jsonpatch` as a requirement for the Ansible `kubernetes.core.k8s` module to manage Kubernetes (K8s) objects:

```
pip install -r requirements.txt
```

#### Step 7: Write an Ansible Play `deploy-to-k8s.yaml` to create a namespace within the EKS cluster and deploy the Nginx application in that namespace via the `nginx.yaml` K8s manifest file:

```
- name: Deploy app in new namespace
  hosts: localhost
  tasks:
    - name: Create a K8s namespace
      kubernetes.core.k8s:
        name: my-app
        api_version: v1
        kind: Namespace
        state: present
        kubeconfig: ../terraform/kubeconfig

    - name: Deploy Nginx app
      kubernetes.core.k8s:
        src: ../kubernetes/nginx.yaml
        state: present
        kubeconfig: ../terraform/kubeconfig
        namespace: my-app
```

![image](images/ansible.png)

##### Check `my-app` namespace is created successful:

```
kubectl get ns
```

![image](images/kubectl-1.png)

###### Check all components deployed in the namespace:

```
kubectl get all -n my-app -o wide
```

![image](images/kubectl-2.png)

###### The Nginx application user interface:

![image](images/nginx.png)
